import std.stdio;

void main(string[] args) {
    for (int i = 0; i < 13; i++) {
        write(i, " ");
        
        for (int j = 1; j < 13; j++) {
            write(i * j, " ");
        }
        
        writeln();
        
    }
}