import std.stdio;
import std.string;
import std.conv;

void main(string[] args) {
    writeln("Enter a number!");
    
    int number = readln().strip().to!int;
    int result = 1;
    
    writeln("Product or sum?");
    
    string answer = readln().strip();
    
    if (answer.toLower() == "sum") {
        for (int i = 1; i < number; i++) {
            result += i;
        }
    } else if (answer.toLower() == "product") {
        for (int j = 1; j < number; j++) {
            result *= j;
        }
    }
    
    writeln("Result: ", result);
    
    getchar();
    
}