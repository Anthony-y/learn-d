import std.stdio;
import std.string;

void main(string[] args) {
    writeln("Enter your name: ");
    
    string name;
    readf("%s\n", &name);
    
    if (name.toLower() == "missfluffy" || name.toLower() == "bob") {
        writeln("Hello, ", name);   
    }
    
    getchar();
}