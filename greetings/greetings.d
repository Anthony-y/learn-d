import std.stdio;

void main(string[] args) {
    writeln("Enter your name: ");
    
    string name;
    readf("%s\n", &name);
    
    writeln("Hello, ", name);
    
    getchar();
}