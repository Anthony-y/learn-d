import std.stdio;
import std.conv;
import std.string;

void main(string[] args) {
    writeln("Enter a number!");
        
    int number = readln().strip().to!int;
    int result = 0;
    
    for (int i = 1; i < number; i++) {
        if (i % 3 == 0 || i % 5 == 0) {
            result += i;
        }
    }
    
    writeln("Wow it worked maybe: ", result);
    
    getchar();
    
}