import std.stdio;

void main(string[] args) {
    //isNumPrime(100);
    
    for (int i = 0; i < 100; i++) {
        if (isNumPrime(i)) {
            writeln("Prime: ", i);
        }
    }
}

bool isNumPrime(int num) {
    if (num <= 1) {
        return false;
    } else if (num <= 3) {
        return true;
    } else if (num % 2 == 0 || num % 3 == 0) {
        return false;
    }
    
    int i = 5;
    
    while (i * i <= num) {
        if (num % i == 0 || num % (i + 2) == 0) {
            return false;
        }
        
        i += 6;
        
    }
    
    return true;
}